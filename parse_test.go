package fmtid

import (
	"testing"

	"gitlab.com/testle/expect"
)

func TestValidPatterns(t *testing.T) {
	r, _ := Parse("Foo")
	expect.Value(t, "parts", r).ToBe([]string{"foo"})

	r, _ = Parse("FooBar")
	expect.Value(t, "parts", r).ToBe([]string{"foo", "bar"})

	r, _ = Parse("FB01")
	expect.Value(t, "parts", r).ToBe([]string{"f", "b01"})

	r, _ = Parse("foo")
	expect.Value(t, "parts", r).ToBe([]string{"foo"})

	r, _ = Parse("fooBar")
	expect.Value(t, "parts", r).ToBe([]string{"foo", "bar"})

	r, _ = Parse("fB01")
	expect.Value(t, "parts", r).ToBe([]string{"f", "b01"})

}

func TestInvalidPatterns(t *testing.T) {
	_, err := Parse("")
	expect.Value(t, "error", err.Error()).ToBe("identifier must not be the empty string")

	_, err = Parse("Fooä")
	expect.Value(t, "error", err.Error()).ToBe("invalid character 'ä' in identifier 'Fooä'")
}
