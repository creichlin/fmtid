package fmtid

import (
	"strings"

	"github.com/jinzhu/inflection"
)

var acronyms = map[string]bool{
	"id":   true,
	"http": true,
	"tcp":  true,
	"ip":   true,
}

func Fmt(id []string, sep string, f func(int, string) string) string {
	npts := []string{}
	for i, v := range id {
		npts = append(npts, f(i, v))
	}
	return strings.Join(npts, sep)
}

func ParseFmt(id string, sep string, f func(int, string) string) (string, error) {
	idp, err := Parse(id)
	if err != nil {
		return "", err
	}
	return Fmt(idp, sep, f), nil
}

func MakeParseFmt(sep string, f func(int, string) string) func(string) (string, error) {
	return func(id string) (string, error) {
		return ParseFmt(id, sep, f)
	}
}

var (
	Kebab      = MakeParseFmt("-", func(i int, s string) string { return s })
	Snake      = MakeParseFmt("_", func(i int, s string) string { return s })
	SnakeUpper = MakeParseFmt("_", func(i int, s string) string { return cap(s) })
	Camel      = MakeParseFmt("", func(i int, s string) string { return u(s, i > 0) })
	CamelUpper = MakeParseFmt("", func(i int, s string) string { return u(s, true) })
	Go         = MakeParseFmt("", func(i int, s string) string { return ug(s, i > 0) })
	GoUpper    = MakeParseFmt("", func(i int, s string) string { return ug(s, true) })
	Lower      = MakeParseFmt("", func(i int, s string) string { return s })
	Upper      = MakeParseFmt("", func(i int, s string) string { return cap(s) })
)

func Plural(s string) (string, error) {
	parts, err := Parse(s)
	if err != nil {
		return "", err
	}
	parts[len(parts)-1] = inflection.Plural(parts[len(parts)-1])
	return Fmt(parts, "", func(i int, s string) string { return u(s, true) }), nil
}

var Formats = map[string]func(string) (string, error){
	"as-id":  Kebab,
	"as_id":  Snake,
	"AS_ID":  SnakeUpper,
	"asId":   Camel,
	"AsId":   CamelUpper,
	"asID":   Go,
	"AsID":   GoUpper,
	"asid":   Lower,
	"ASID":   Upper,
	"plural": Plural,
}

func ug(i string, do bool) string {
	if do {
		if acronyms[i] {
			return strings.ToUpper(i)
		}
		return strings.ToUpper(i[0:1]) + i[1:]
	}
	return i
}

func u(i string, do bool) string {
	if do {
		return strings.ToUpper(i[0:1]) + i[1:]
	}
	return i
}

var cap = strings.ToUpper
