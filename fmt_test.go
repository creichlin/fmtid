package fmtid

import (
	"testing"

	"gitlab.com/testle/expect"
)

func TestFmt(t *testing.T) {
	expect.Value(t, "formats", eval(t, "Foo")).ToBe(map[string]string{
		"AsID":   "Foo",
		"AsId":   "Foo",
		"as-id":  "foo",
		"as_id":  "foo",
		"AS_ID":  "FOO",
		"asID":   "foo",
		"asId":   "foo",
		"asid":   "foo",
		"ASID":   "FOO",
		"plural": "Foos",
	})
	expect.Value(t, "formats", eval(t, "FooBar")).ToBe(map[string]string{
		"AsID":   "FooBar",
		"AsId":   "FooBar",
		"as-id":  "foo-bar",
		"as_id":  "foo_bar",
		"AS_ID":  "FOO_BAR",
		"asID":   "fooBar",
		"asId":   "fooBar",
		"asid":   "foobar",
		"ASID":   "FOOBAR",
		"plural": "FooBars",
	})
	expect.Value(t, "formats", eval(t, "FooIdBar")).ToBe(map[string]string{
		"AsID":   "FooIDBar",
		"AsId":   "FooIdBar",
		"as-id":  "foo-id-bar",
		"as_id":  "foo_id_bar",
		"AS_ID":  "FOO_ID_BAR",
		"asID":   "fooIDBar",
		"asId":   "fooIdBar",
		"asid":   "fooidbar",
		"ASID":   "FOOIDBAR",
		"plural": "FooIdBars",
	})
}

func eval(t *testing.T, id string) map[string]string {
	vals := map[string]string{}
	for k, f := range Formats {
		res, err := f(id)
		if err != nil {
			t.Fatal(err)
		}
		vals[k] = res
	}
	return vals
}
