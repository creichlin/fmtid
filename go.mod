module gitlab.com/creichlin/fmtid

go 1.13

require (
	github.com/jinzhu/inflection v1.0.0
	gitlab.com/testle/expect v0.0.0-20200309150905-ff68817b3202
)
