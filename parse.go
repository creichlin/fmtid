package fmtid

import (
	"errors"
	"fmt"
	"strings"
)

const (
	otherChars = "abcdefghijklmnopqrstuvwxyz0123456789"
	firstChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

func Parse(n string) ([]string, error) {
	if n == "" {
		return nil, errors.New("identifier must not be the empty string")
	}

	tokens := []string{}
	token := n[0:1]

	for i, r := range n {
		if i == 0 {
			continue
		}
		ch := string(r)
		if strings.Contains(firstChars, ch) {
			tokens = append(tokens, strings.ToLower(token))
			token = ch
			continue
		}
		if strings.Contains(otherChars, ch) {
			token += ch
			continue
		}
		return nil, fmt.Errorf("invalid character '%v' in identifier '%v'", ch, n)
	}
	tokens = append(tokens, strings.ToLower(token))

	return tokens, nil
}
