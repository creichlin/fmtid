# fmtid

Moved to [gitlab.com/akabio/fmtid](https://gitlab.com/akabio/fmtid)

formats identifiers into different name schemas like

- camelCase
- kebab-case
- snake_case

As input it takes an identifier in lower or upper camelCase with acronyms
first upper and rest lowercase. The canonical form is UpperCamelCase.

## purpose

It's intended purpose is to generate code for different data and programming languages.

The Identifier `customerId` is in go expected to be `customerID` or `CustomerID` whereas in json it's common to use `customerId` whereas in many languages as a constant it would be `CUSTOMER_ID`.

## api

The functions take an input string in camel case and turn it into it's expected
output. They are also available inside a map so it will be easier to add them as filters to a template language. The map keys use the name `asId` in the target format:

    "as-id"  Kebab(...)
    "as_id"  Snake(...)
    "AS_ID"  UpperSnake(...)
    "asId"   Camel(...)
    "AsId"   UpperCamel(...)
    "asID"   Go(...)
    "AsID"   UpperGo(...)
    "asid"   Lower(...)
    "ASID"   Upper(...)
    "plural" Plural(...)
